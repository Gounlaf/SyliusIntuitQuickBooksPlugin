<?php

declare(strict_types=1);

namespace Tests\Gounlaf\SyliusIntuitQuickBooksPlugin\Unit;

use QuickBooksOnline\API\DataService\DataService;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class CreateSdkTest extends KernelTestCase
{
    protected function setUp()
    {
        parent::setUp();
        self::bootKernel();
    }

    public function testFactory()
    {
        $publicContainer = self::$kernel->getContainer();
        $testContainer = self::$container;

        $this->assertTrue($publicContainer->has('gounlaf_sylius_intuit_quickbooks_plugin.sdk'), 'public container has private service');
        $this->assertTrue($testContainer->has('gounlaf_sylius_intuit_quickbooks_plugin.sdk'), 'test container has private service');

        $sdk = self::$container->get('gounlaf_sylius_intuit_quickbooks_plugin.sdk');
        $this->assertInstanceOf(DataService::class, $sdk);
    }
}
