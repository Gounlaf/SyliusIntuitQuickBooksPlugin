FROM phpearth/php:7.2-cli

RUN apk add --no-cache php7.2-bcmath
RUN apk add --no-cache php7.2-curl
RUN apk add --no-cache php7.2-exif
RUN apk add --no-cache php7.2-gd
RUN apk add --no-cache php7.2-intl
RUN apk add --no-cache php7.2-json
RUN apk add --no-cache php7.2-mbstring
RUN apk add --no-cache php7.2-openssl
RUN apk add --no-cache php7.2-pdo
RUN apk add --no-cache composer
RUN apk add --no-cache phpunit

COPY . /app
WORKDIR /app

RUN composer install
